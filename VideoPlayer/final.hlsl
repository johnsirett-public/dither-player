/* VERTEX SHADER */

cbuffer Constants : register(b0)
{
	uint enableUpscale;
	float4 aspectAdjust;
};

struct VSToPS
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD;
};

VSToPS main_vs(float4 position : POSITION, float2 uv : TEXCOORD)
{
	VSToPS output;

	output.position = float4(position.xy*aspectAdjust.xy + aspectAdjust.zw, position.zw);
	output.uv = uv;

	return output;
}

/* PIXEL SHADER */

texture2D filterTexture : register(t0);
SamplerState filterSampler : register(s0);

// Lanczos4 filtering
#define FIX(c) max(abs(c), 1e-5)
static float PI = 3.1415926535897932384626433832795;
float4 weight4(float x)
{
	float4 smp = FIX(PI * float4(1.0f + x, x, 1.0f - x, 2.0f - x));
	float4 ret = sin(smp) * sin(smp / 2.0f) / (smp * smp);
	return ret / dot(ret, float4(1.0f, 1.0f, 1.0f, 1.0f));
}
float3 pixel(float xpos, float ypos, texture2D tex)
{
	return tex.Sample(filterSampler, float2(xpos, ypos)).rgb;
}
float3 line_run(float ypos, float4 xpos, float4 linetaps, texture2D tex)
{
	return mul(linetaps, float4x3(
		pixel(xpos.x, ypos, tex),
		pixel(xpos.y, ypos, tex),
		pixel(xpos.z, ypos, tex),
		pixel(xpos.w, ypos, tex)));
}
float3 lanczos4(float2 textureSize, float2 texCoord, texture2D tex)
{
	float2 stepxy = 1.0f / textureSize.xy;
	float2 pos = texCoord.xy + stepxy * 0.5f;
	float2 f = frac(pos / stepxy);

	float2 xystart = (-1.5f - f) * stepxy + pos;
	float4 xpos = float4(
		xystart.x,
		xystart.x + stepxy.x,
		xystart.x + stepxy.x * 2.0,
		xystart.x + stepxy.x * 3.0
		);

	float4 linetaps = weight4(f.x);
	float4 columntaps = weight4(f.y);

	return float3(mul(columntaps, float4x3(
		line_run(xystart.y                  , xpos, linetaps, tex),
		line_run(xystart.y + stepxy.y       , xpos, linetaps, tex),
		line_run(xystart.y + stepxy.y * 2.0f, xpos, linetaps, tex),
		line_run(xystart.y + stepxy.y * 3.0f, xpos, linetaps, tex))));
}

float4 main_ps(VSToPS input) : SV_TARGET
{
	float2 textureSize;
	filterTexture.GetDimensions(textureSize.x, textureSize.y);

	float3 result;
	if (enableUpscale) result = lanczos4(textureSize, input.uv, filterTexture);
	else result = filterTexture.Sample(filterSampler, input.uv).xyz;

	return float4(result, 1.0f);
}