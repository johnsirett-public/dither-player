/* VERTEX SHADER */

struct VSToPS
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD;
};

VSToPS main_vs(float4 position : POSITION, float2 uv : TEXCOORD)
{
	VSToPS output;

	output.position = position;
	output.uv = uv;

	return output;
}

/* PIXEL SHADER */

texture2D sourceTexture : register(t0);
SamplerState sourceSampler : register(s0);

float4 main_ps(VSToPS input) : SV_TARGET
{
	float3 color = sourceTexture.Sample(sourceSampler, input.uv).xyz;
	return float4(color, 1.0f);
}