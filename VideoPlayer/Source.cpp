#ifndef UNICODE
#define UNICODE
#endif

#include <sstream>
#include <memory>
#include <vector>
#include <algorithm>
#include <functional>

#include "rapidfuzz/fuzz.hpp"

#include <Windows.h>
#include <wrl/client.h>
#include <dwmapi.h>
#include <d3d11.h>
#include <DirectXMath.h>
#include <d3dcompiler.h>
#include <winrt/Windows.Foundation.Collections.h>
#include <winrt/Windows.Graphics.Capture.h>
#include <Windows.Graphics.Capture.Interop.h>
#include <windows.graphics.directx.direct3d11.interop.h>

#pragma comment (lib, "dwmapi.lib")
#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "D3DCompiler.lib")
#pragma comment (lib, "windowsapp")

using namespace DirectX;
using Windows::Graphics::DirectX::Direct3D11::IDirect3DDxgiInterfaceAccess;
namespace fuzz = rapidfuzz::fuzz;

namespace WRT = winrt;
namespace WF = WRT::Windows::Foundation;
namespace WG = WRT::Windows::Graphics;
namespace WGC = WG::Capture;
namespace WGDX = WG::DirectX;
namespace WGD3D11 = WGDX::Direct3D11;

using std::shared_ptr, std::make_shared;
using Microsoft::WRL::ComPtr;

// Defines and typedefs.
enum { NOMINAL_VIEW_WIDTH = 1280, NOMINAL_VIEW_HEIGHT = 720 };
enum { MAX_PALETTE_SIZE = 1024 };

// Function prototypes.
LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

class WGCCapture
{
	// Stored captured window
	HWND capturedWindow;

	// WGC Interfaces
	WGC::GraphicsCaptureItem item;
	WGD3D11::IDirect3DDevice device;
	WGC::Direct3D11CaptureFramePool framePool;
	WGC::GraphicsCaptureSession session;
	WG::SizeInt32 lastSize;

	// WGC Callback Management
	WGC::Direct3D11CaptureFramePool::FrameArrived_revoker frameArrived;

	// Frame surface
	ComPtr<ID3D11Texture2D> frameSurface;
	bool frameSurfaceReady;

public:
	enum class CaptureReadiness { CAPTURE_NOT_READY, CAPTURE_READY, CAPTURE_DEAD };

	void OnFrameArrived(WGC::Direct3D11CaptureFramePool const& sender, WF::IInspectable const &)
	{
		// Get the last captured frame.

		const WGC::Direct3D11CaptureFrame frame = sender.TryGetNextFrame();
		const WG::SizeInt32 contentSize = frame.ContentSize();
		
		// Extract the frame surface and mark as ready.

		ComPtr<ID3D11Texture2D> newFrameSurface;
		auto frameSurfaceAccess = frame.Surface().as<IDirect3DDxgiInterfaceAccess>();
		frameSurfaceAccess->GetInterface(WRT::guid_of<ID3D11Texture2D>(), (void**)newFrameSurface.GetAddressOf());

		frameSurface = newFrameSurface;
		frameSurfaceReady = true;

		// Handle resize of captured window.
		if (contentSize.Width != lastSize.Width || contentSize.Height != lastSize.Height)
		{
			framePool.Recreate(device, WGDX::DirectXPixelFormat::B8G8R8A8UIntNormalized, 2, contentSize);

			lastSize = contentSize;
		}
	}

	// Client: Call IsCaptureReady() to check if a new frame capture is available. Call CaptureTaken() after copying the data.
	CaptureReadiness IsCaptureReady()
	{
		if (!capturedWindow || !IsWindow(capturedWindow) || !IsWindowVisible(capturedWindow))
			return CaptureReadiness::CAPTURE_DEAD;
		else
			return frameSurfaceReady ? CaptureReadiness::CAPTURE_READY : CaptureReadiness::CAPTURE_NOT_READY;
	}
	void CaptureTaken() { frameSurfaceReady = false; }

	ComPtr<ID3D11Texture2D> GetFrameSurface()
	{
		if (!frameSurfaceReady)
			return nullptr;
		
		return frameSurface;
	}

	D3D11_TEXTURE2D_DESC GetFrameSurfaceDesc()
	{
		D3D11_TEXTURE2D_DESC desc = {};

		if (!frameSurfaceReady)
			return desc;

		frameSurface->GetDesc(&desc);
		return desc;
	}

	D3D11_BOX GetClientSubarea()
	{
		D3D11_BOX clientBox = {};

		// Get dimensions for texture.
		D3D11_TEXTURE2D_DESC desc = GetFrameSurfaceDesc();
		uint32_t surfaceWidth = desc.Width;
		uint32_t surfaceHeight = desc.Height;

		// Setting the process as per-monitor DPI aware will do this automatically,
		//	and is needed to get the right values from GetClientRect() and ClientToScreen() later.
		SetProcessDpiAwarenessContext(DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE);
		//float windowDPIFactor = GetDpiForWindow(capturedWindow) / 96.0f;

		// Get dimensions for captured window.
		// Client rect needs to be scaled per window DPI factor (this is done automcatilly based on DPI awareness context).
		// Window rect is a position on screen in raw pixels (not DPI scaled).
		RECT clientRect = {}, windowRect = {};
		GetClientRect(capturedWindow, &clientRect);
		DwmGetWindowAttribute(capturedWindow, DWMWA_EXTENDED_FRAME_BOUNDS, &windowRect, sizeof(windowRect));

		// Calculate upper left point.
		POINT upperLeft = { 0 };
		ClientToScreen(capturedWindow, &upperLeft);

		// Calculate left/top for client box.
		const uint32_t left = (upperLeft.x > windowRect.left) ? (upperLeft.x - windowRect.left) : 0;
		clientBox.left = left;

		const uint32_t top = (upperLeft.y > windowRect.top) ? (upperLeft.y - windowRect.top) : 0;
		clientBox.top = top;

		// Calculate width/height of client, and remaining dimension of client box.
		uint32_t clientWidth = 1;
		if (surfaceWidth > left)
			clientWidth = min((surfaceWidth - left), (uint32_t)clientRect.right);
		uint32_t clientHeight = 1;
		if (surfaceHeight > top)
			clientHeight = min((surfaceHeight - top), (uint32_t)clientRect.bottom);

		clientBox.right = left + clientWidth;
		clientBox.bottom = top + clientHeight;
		
		clientBox.front = 0;
		clientBox.back = 1;

		return clientBox;
	}

	WGCCapture(const ComPtr<IDXGIDevice>& dxgiDevice, HWND inCapturedWindow)
		: capturedWindow(inCapturedWindow),
		  item(nullptr), device(nullptr), framePool(nullptr), session(nullptr), frameSurfaceReady(false), lastSize()
	{
		assert(WGC::GraphicsCaptureSession::IsSupported()); // Assert WGC support
		if (!capturedWindow || !IsWindow(capturedWindow) || !IsWindowVisible(capturedWindow))
			return;

		// Get a GraphicsCaptureItem for the window.
		// This feels kind of hacky, but is indeed the 'correct' way to do this.

		auto activationFactory = WRT::get_activation_factory<WGC::GraphicsCaptureItem>();
		auto gciInterop = activationFactory.as<IGraphicsCaptureItemInterop>();
		gciInterop->CreateForWindow(capturedWindow, WRT::guid_of<WGC::GraphicsCaptureItem>(), (void**)WRT::put_abi(item));

		// Get a WinRT D3D11 device from given DXGI device.

		WRT::com_ptr<IInspectable> inspectable;
		CreateDirect3D11DeviceFromDXGIDevice(dxgiDevice.Get(), inspectable.put());
		device = inspectable.as<WGD3D11::IDirect3DDevice>();

		// Create capture frame pool and session, and register callbacks.

		framePool = WGC::Direct3D11CaptureFramePool::Create(device, WGDX::DirectXPixelFormat::B8G8R8A8UIntNormalized, 2, item.Size());
		session = framePool.CreateCaptureSession(item);
		lastSize = item.Size();
		frameArrived = framePool.FrameArrived(WRT::auto_revoke, { this, &WGCCapture::OnFrameArrived});

		// Start the capture.

		session.StartCapture();
	}
};

class PaletteGeneration
{
protected:
	// Size of palette.
	unsigned paletteSize = 0;

	// Triggers palette updates for palette generators that have 'sticky' aspects.
	bool forceUpdate = false;

	// Type of work palette entry -- just color.
	enum { RED, GREEN, BLUE, _PAD };
	typedef struct { float color[3]; } PALETTE_TYPE;

	// Euclidian color distance utility.
	float ColorDistance(PALETTE_TYPE a, PALETTE_TYPE b)
	{
		float rDiff = b.color[RED] - a.color[RED];
		float gDiff = b.color[GREEN] - a.color[GREEN];
		float bDiff = b.color[BLUE] - a.color[BLUE];
		return sqrtf((rDiff * rDiff) + (gDiff * gDiff) + (bDiff * bDiff));
	}
public:
	// Returns the number of colors actually used in palette. Parameters should be self-explanatory.
	virtual unsigned CalcPaletteFromFrame(const BYTE* frameData, unsigned frameWidth, unsigned frameHeight, XMFLOAT4* outPalette) = 0;
	virtual XMFLOAT3 GetPaletteThresholds() = 0;
	unsigned GetMaxPaletteSize() { return this->paletteSize; }
	void ForceUpdate() { forceUpdate = true; }
};

class DummyPaletteGeneration : public PaletteGeneration
{
	unsigned CalcPaletteFromFrame(const BYTE* frameData, unsigned frameWidth, unsigned frameHeight, XMFLOAT4* outPalette) { return 0; }
	XMFLOAT3 GetPaletteThresholds() { return XMFLOAT3(0, 0, 0); }
};

class FixedPaletteGeneration : public PaletteGeneration
{
private:
	PALETTE_TYPE* palette; // Internal pre-converted palette for output.
	PALETTE_TYPE* threshPalette; // Sortable palette for threshold calculation.
	PALETTE_TYPE paletteThresholds; // Pre-computed palette thresholds. Stored as single palette color.

public:
	FixedPaletteGeneration(const std::vector<BYTE>& fixedPalette)
	{
		assert(fixedPalette.size() % 3 == 0);
		assert(fixedPalette.size() < MAX_PALETTE_SIZE * 3);

		// Allocate internal palette sotrage.
		paletteSize = (unsigned)(fixedPalette.size() / 3);
		palette = (PALETTE_TYPE*)malloc(paletteSize * sizeof(PALETTE_TYPE));
		threshPalette = (PALETTE_TYPE*)malloc(paletteSize * sizeof(PALETTE_TYPE));

		// Convert to internal float palette.
		for (unsigned i = 0; i < paletteSize; i++) {
			for (unsigned j = 0; j < 3; j++) {
				palette[i].color[j] = float(fixedPalette[(i * 3) + j]) / 256.0f;
			}
		}

		// Compute palette thresholds.
		memcpy(threshPalette, palette, paletteSize * sizeof(PALETTE_TYPE));
		paletteThresholds = { 0.0f, 0.0f, 0.0f };
		// threshMax is used to restrict the palette to a maximum for saner results on small palettes.
		float threshMax = 0.5f;
		for (unsigned j = 0; j < 3; j++) {
			std::sort(threshPalette, threshPalette + paletteSize, [j](PALETTE_TYPE a, PALETTE_TYPE b) {
				return a.color[j] < b.color[j];
			});

			for (unsigned i = 0; i < paletteSize - 1; i++) {
				float succDist = threshPalette[i + 1].color[j] - threshPalette[i].color[j];
				if (succDist > paletteThresholds.color[j])
					paletteThresholds.color[j] = min(threshMax, succDist);
			}
		}
	}

	~FixedPaletteGeneration()
	{
		free(palette);
	}

	unsigned CalcPaletteFromFrame(const BYTE* frameData, unsigned frameWidth, unsigned frameHeight, XMFLOAT4* outPalette)
	{
		for (unsigned i = 0; i < paletteSize; i++) {
			outPalette[i] = XMFLOAT4(palette[i].color[RED], palette[i].color[GREEN], palette[i].color[BLUE], 0.0f);
		}
		return this->paletteSize;
	}
	XMFLOAT3 GetPaletteThresholds()
	{
		return XMFLOAT3(paletteThresholds.color[RED], paletteThresholds.color[GREEN], paletteThresholds.color[BLUE]);
	}

	unsigned GetFixedPalette(XMFLOAT4* outPalette) { return CalcPaletteFromFrame(nullptr, 0, 0, outPalette); }
	PALETTE_TYPE GetClosestFromFixedPalette(PALETTE_TYPE color)
	{
		PALETTE_TYPE paletteEntry;
		PALETTE_TYPE closest = palette[0];
		for (unsigned i = 0; i < paletteSize; ++i) {
			paletteEntry = palette[i];
			float newDist = ColorDistance(color, paletteEntry);
			if (newDist < ColorDistance(color, closest))
			{
				closest = paletteEntry;
			}
		}

		return closest;
	}
};

class MCPaletteGeneration : public PaletteGeneration
{
private:
	// Type of input histogram entry (byte color + count).
	typedef struct
	{
		BYTE color[3];
		uint32_t count;
	} IN_HISTO_TYPE;
	typedef std::vector<IN_HISTO_TYPE>::iterator IN_HISTO_ITER;

	// Type of output histogram entry (palette entry + count).
	typedef struct
	{
		PALETTE_TYPE entry;
		uint32_t count;
	} OUT_HISTO_TYPE;
	typedef std::vector<OUT_HISTO_TYPE>::iterator OUT_HISTO_ITER;

	// Input generation parameters.
	unsigned rDepth, gDepth, bDepth;
	unsigned useResident;

	// Shift parameters for float conversion.
	unsigned rShift, gShift, bShift;
	unsigned rPlace, gPlace, bPlace;
	unsigned rMask, gMask, bMask;

	// Internal algorithm data.
	unsigned mcPaletteSize;        // Power-of-two palette size to use for algorithm.
	unsigned maxLevel;             // Maximum level for median cut algorithm.
	unsigned residentPaletteSize;  // Size of the resident palette below.
	PALETTE_TYPE* residentPalette; // Resident palette -- will always be output in some section of the palette space.

	// Internal palette data.
	uint32_t* colorspace;          // Simple array for fast counting of colors in frame.
	unsigned colorspaceSize;       // Size of our colorspace (or basis palette).
	std::vector<IN_HISTO_TYPE> workHisto;    // Working color + count histogram.
	OUT_HISTO_TYPE* workPalette;     // Working palette for new frame.
	OUT_HISTO_TYPE* currentPalette;  // Saved palette for output.
	OUT_HISTO_TYPE* threshPalette;   // Resortable palette for threshold calculation.

	IN_HISTO_ITER MCCalcExactMedian(IN_HISTO_ITER begin, IN_HISTO_ITER end)
	{
		return begin + ((end - begin) / 2) + 1;
	}

	void MedianCutRecursive(IN_HISTO_ITER begin, IN_HISTO_ITER end, int palLo, int palHi, unsigned level)
	{
		// Check denegrate iterators.
		if (begin == end)
			return;

		// Generate the new mins and maxs for our range
		BYTE mins[3], maxs[3];
		uint32_t localCount = 0;
		memset(mins, 255, 3 * sizeof(BYTE));
		memset(maxs, 0, 3 * sizeof(BYTE));

		for (IN_HISTO_ITER i = begin; i != end; i++) {
			for (unsigned j = 0; j < 3; j++) {
				mins[j] = min(mins[j], i->color[j]);
				maxs[j] = max(maxs[j], i->color[j]);
			}
			localCount += i->count;
		}

		// Base case -- emit colors.
		if (level == this->maxLevel)
		{
			IN_HISTO_ITER best = begin;
			uint32_t bestCount = 0;
			for (IN_HISTO_ITER i = begin; i != end; i++) {
				if (i->count > bestCount) {
					best = i; bestCount = i->count;
				}	
			}

			workPalette[palLo].entry.color[RED]   = float(best->color[RED])     / (float)rMask;
			workPalette[palLo].entry.color[GREEN] = float(best->color[GREEN])   / (float)gMask;
			workPalette[palLo].entry.color[BLUE]  = float(best->color[BLUE])    / (float)bMask;
			workPalette->count = localCount;

			return;
		}

		// Select greatest range.
		unsigned colorIdx = GREEN;
		if ((maxs[RED] - mins[RED]) > (maxs[GREEN] - mins[GREEN]))
			colorIdx = RED;
		if ((maxs[BLUE] - mins[BLUE]) > (maxs[colorIdx] - mins[colorIdx]))
			colorIdx = BLUE;

		// Sort on color channel.
		std::sort(begin, end, [colorIdx](IN_HISTO_TYPE a, IN_HISTO_TYPE b) {
			return a.color[colorIdx] < b.color[colorIdx];
		});

		// Recurse the buckets.
		IN_HISTO_ITER median = MCCalcExactMedian(begin, end);
		MedianCutRecursive(begin, median, palLo, palLo + ((palHi - palLo) / 2), level + 1);
		MedianCutRecursive(median, end, palLo + ((palHi - palLo) / 2), palHi, level + 1);
	}

public:
	MCPaletteGeneration(unsigned inPaletteSize = 256, unsigned inRDepth = 6, unsigned inGDepth = 6, unsigned inBDepth = 6, const std::vector<BYTE> inResidentPalette = std::vector<BYTE>())
		: rDepth(inRDepth), gDepth(inGDepth), bDepth(inBDepth)
	{
		paletteSize = inPaletteSize;
		useResident = inResidentPalette.size() > 0;
		
		assert(rDepth >= 1 && rDepth <= 8); assert(gDepth >= 1 && gDepth <= 8); assert(bDepth >= 1 && bDepth <= 8);
		assert(paletteSize >= 2 && paletteSize <= MAX_PALETTE_SIZE); // Palette size is a good value

		residentPaletteSize = 0;
		if (useResident)
		{
			// Allocate resident palette storage.
			residentPaletteSize = (unsigned)(inResidentPalette.size() / 3);
			residentPalette = (PALETTE_TYPE*)malloc(paletteSize * sizeof(PALETTE_TYPE));
			assert(residentPaletteSize < paletteSize);

			// Convert the resident palette to internal float palette.
			for (unsigned i = 0; i < residentPaletteSize; i++) {
				for (unsigned j = 0; j < 3; j++) {
					residentPalette[i].color[j] = float(inResidentPalette[(i * 3) + j]) / 256.0f;
				}
			}
		}

		// Calculate end level of median cut and internal palette size.
		mcPaletteSize = 1 << (int(log2f(float(paletteSize - residentPaletteSize)) + .5f));
		maxLevel = int(log2f(mcPaletteSize + 0.5f));
		assert((1 << (maxLevel)) == mcPaletteSize);

		// Calculate shifting parameters.
		rShift = (8 - rDepth), gShift = (8 - gDepth), bShift = (8 - bDepth);
		rPlace = bDepth + gDepth, gPlace = bDepth, bPlace = 0;
		rMask = (1 << rDepth) - 1, gMask = (1 << gDepth) - 1, bMask = (1 << bDepth) - 1;

		// Allocate the color counter array.
		colorspaceSize = (1 << rDepth) * (1 << gDepth) * (1 << bDepth);
		colorspace = (uint32_t*)malloc(colorspaceSize * sizeof(uint32_t));

		// Allocate the work palettes -- 3 bytes per entry.
		workPalette = (OUT_HISTO_TYPE*)calloc(mcPaletteSize, sizeof(OUT_HISTO_TYPE));
		currentPalette = (OUT_HISTO_TYPE*)calloc(mcPaletteSize, sizeof(OUT_HISTO_TYPE));
		threshPalette = (OUT_HISTO_TYPE*)calloc(mcPaletteSize, sizeof(OUT_HISTO_TYPE));
	}

	~MCPaletteGeneration()
	{
		if (useResident)
			free(residentPalette);

		free(colorspace);
		free(workPalette);
		free(currentPalette);
		free(threshPalette);
	}

	unsigned CalcPaletteFromFrame(const BYTE* frameData, unsigned frameWidth, unsigned frameHeight, XMFLOAT4* outPalette)
	{
		// Check the color + count histogram is large enough to stop reallocations.
		unsigned histoSize = min(frameWidth * frameHeight, colorspaceSize);
		workHisto.reserve(histoSize);

		// Count frequencies of colors in the frame.
		memset(colorspace, 0, colorspaceSize * sizeof(uint32_t));
		BYTE r, g, b; uint32_t color;
		for (unsigned i = 0; i < frameWidth; i++) {
			for (unsigned j = 0; j < frameHeight; j++) {
				r = (*frameData++); g = (*frameData++); b = (*frameData++);
				frameData++; // Skip alpha.

				color = ((r >> rShift) << rPlace);
				color += ((g >> gShift) << gPlace);
				color += ((b >> bShift) << bPlace);
				colorspace[color] += 1;
			}
		}

		// Fill a new color + count histogram for this frame.
		workHisto.clear();
		IN_HISTO_TYPE elem;
		for (unsigned i = 0; i < colorspaceSize; i++) {
			if (colorspace[i] != 0)
			{
				r = ((i >> rPlace) & rMask);
				g = ((i >> gPlace) & gMask);
				b = ((i >> bPlace) & bMask);
				
				elem.color[RED] = r; elem.color[GREEN] = g; elem.color[BLUE] = b;
				elem.count = colorspace[i];
				workHisto.push_back(elem);
			}
		}

		// Optimize work palette using median cut algorithm.
		MedianCutRecursive(workHisto.begin(), workHisto.end(), 0, mcPaletteSize, 0);

		// Now work out the similarity between the old palette and new palette (Bhattacharyya).
		float paletteDisassociation = 0.0f, s1 = 0.0f, s2 = 0.0f;
		PALETTE_TYPE workColor, threshColor;
		for (unsigned i = 0; i < paletteSize; i++) {
			workColor = workPalette[i].entry;

			// Find best 'paired' color. Yes this is o(n^2), but the n is usually at most 256, so we'll prolly get away with it.
			float bestDistance = FLT_MAX, newDistance;
			unsigned bestIdx;
			for (unsigned j = 0; j < paletteSize; j++) {
				threshColor = threshPalette[j].entry;
				newDistance = ColorDistance(workColor, threshColor);
				if (newDistance < bestDistance) {
					bestDistance = newDistance;
					bestIdx = j;
				}
			}

			// Now accumulate counts for Bhattacharyya.
			float a = (float)workPalette[i].count / (float)histoSize;
			float b = (float)threshPalette[bestIdx].count / (float)histoSize;
			paletteDisassociation += std::sqrt(a*b);
			s1 += a;
			s2 += b;
		}
		// Finalize Bhattacharyya.
		s1 *= s2;
		s1 = fabs(s1) > FLT_EPSILON ? 1.0f / std::sqrt(s1) : 1.0f;
		paletteDisassociation = std::sqrt(max(1.0f - paletteDisassociation * s1, 0.0f));

		// Copy through the palette only if it is sufficiently different.
		if (paletteDisassociation > 0.67f || forceUpdate) {
			memcpy(currentPalette, workPalette, mcPaletteSize * sizeof(OUT_HISTO_TYPE));
			memcpy( threshPalette, workPalette, mcPaletteSize * sizeof(OUT_HISTO_TYPE));

			std::sort(currentPalette, currentPalette + mcPaletteSize, [](OUT_HISTO_TYPE a, OUT_HISTO_TYPE b) {
				return a.count < b.count;
				});

			forceUpdate = false;
		}

		// Output requisite number of colors from our internal output palette.
		unsigned i = 0, idx;
		if (useResident)
			for (; i < residentPaletteSize; i++)
				outPalette[i] = XMFLOAT4(residentPalette[i].color[RED], residentPalette[i].color[GREEN], residentPalette[i].color[BLUE], 0.0f);
		for (; i < paletteSize; i++) {
			idx = i - residentPaletteSize;
			outPalette[i] = XMFLOAT4(currentPalette[idx].entry.color[RED], currentPalette[idx].entry.color[GREEN], currentPalette[idx].entry.color[BLUE], 0.0f);
		}
			

		return paletteSize;
	}

	XMFLOAT3 GetPaletteThresholds()
	{
		// Compute palette thresholds.
		PALETTE_TYPE newPaletteThresholds = { 0.0f, 0.0f, 0.0f };
		for (unsigned j = 0; j < 3; j++) {
			std::sort(threshPalette, threshPalette + mcPaletteSize, [j](OUT_HISTO_TYPE a, OUT_HISTO_TYPE b) {
				return a.entry.color[j] < b.entry.color[j];
			});

			for (unsigned i = 0; i < mcPaletteSize - 1; i++) {
				float succDist = threshPalette[i + 1].entry.color[j] - threshPalette[i].entry.color[j];
				if (succDist > newPaletteThresholds.color[j])
					newPaletteThresholds.color[j] = succDist;
			}
		}
		return XMFLOAT3(newPaletteThresholds.color[RED], newPaletteThresholds.color[GREEN], newPaletteThresholds.color[BLUE]);
	}
};

class MCWithBasisPaletteGeneration : public PaletteGeneration
{
private:
	MCPaletteGeneration mcGen;       // Generates initial optimized palette.
	XMFLOAT4* optimPalette;          // Storage for optimized palette.
	FixedPaletteGeneration fixedGen; // Acts as storage for the basisPalette.
public:
	MCWithBasisPaletteGeneration(unsigned inPaletteSize, const std::vector<BYTE>& basisPalette)
		: mcGen(paletteSize), fixedGen(basisPalette)
	{
		paletteSize = inPaletteSize;
		assert(inPaletteSize < basisPalette.size()); // We have enough colors in basis palette to satisfy.

		optimPalette = (XMFLOAT4*)malloc(paletteSize * sizeof(XMFLOAT4));
	}

	~MCWithBasisPaletteGeneration()
	{
		free(optimPalette);
	}

	unsigned CalcPaletteFromFrame(const BYTE* frameData, unsigned frameWidth, unsigned frameHeight, XMFLOAT4* outPalette)
	{
		// First run the optimization.
		
		mcGen.CalcPaletteFromFrame(frameData, frameWidth, frameHeight, optimPalette);

		// For each color in optimized palette, output closest colors from fixed palette.
		PALETTE_TYPE entryFromBasis;
		for (unsigned i = 0; i < paletteSize; i++) {
			entryFromBasis = fixedGen.GetClosestFromFixedPalette({ optimPalette[i].x, optimPalette[i].y, optimPalette[i].z });
			outPalette[i] = XMFLOAT4(entryFromBasis.color[RED], entryFromBasis.color[GREEN], entryFromBasis.color[BLUE], 0.0f);
		}

		return paletteSize;
	}
	XMFLOAT3 GetPaletteThresholds() { return fixedGen.GetPaletteThresholds(); }
};

#include "FixedPalettes.h" // This include must happen here, after all palette classes are defined.

template <typename T>
class Direct3DConstants
{
	typedef T CONSTANTS_TYPE;

	// Direct3D Overall Interfaces
	ComPtr<ID3D11Device> device;
	ComPtr<ID3D11DeviceContext> deviceContext;

	// Constant buffer structures
	bool isImmutable;
	ComPtr<ID3D11Buffer> buffer;

public:
	Direct3DConstants(const ComPtr<ID3D11Device>& inDevice,
		const ComPtr<ID3D11DeviceContext>& inDeviceContext,
	    const CONSTANTS_TYPE* initialData = nullptr,
		const bool inImmutable = false
		)
		: device(inDevice), deviceContext(inDeviceContext), isImmutable(inImmutable)
	{
		// Check alignment.
		static_assert(sizeof(CONSTANTS_TYPE) % 16 == 0, "Constants structure not aligned to 16-byte boundary.");

		// Create constants buffer.

		D3D11_BUFFER_DESC bd = {};
		bd.Usage = this->isImmutable ? D3D11_USAGE_IMMUTABLE : D3D11_USAGE_DYNAMIC;
		bd.ByteWidth = sizeof(CONSTANTS_TYPE);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = this->isImmutable ? 0 : D3D11_CPU_ACCESS_WRITE;

		if (initialData != nullptr)
		{
			D3D11_SUBRESOURCE_DATA srd = {};
			srd.pSysMem = initialData;
			this->device->CreateBuffer(&bd, &srd, this->buffer.ReleaseAndGetAddressOf());
		}
		else
		{
			this->device->CreateBuffer(&bd, nullptr, this->buffer.ReleaseAndGetAddressOf());
		}
	}
	
	void UploadData(const CONSTANTS_TYPE* newData)
	{
		D3D11_MAPPED_SUBRESOURCE ms;
		this->deviceContext->Map(this->buffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &ms);
		memcpy(ms.pData, newData, sizeof(CONSTANTS_TYPE));
		this->deviceContext->Unmap(this->buffer.Get(), 0);
	}

	void BindPS(UINT slot) { this->deviceContext->PSSetConstantBuffers(slot, 1, buffer.GetAddressOf()); }
	void BindVS(UINT slot) { this->deviceContext->VSSetConstantBuffers(slot, 1, buffer.GetAddressOf()); }
};


class Direct3DShaderPass
{
	enum class SHADER_TYPE
	{
		ST_VERTEXSHADER,
		ST_PIXELSHADER
	};

	// Direct3D Overall Interfaces
	ComPtr<ID3D11Device> device;
	ComPtr<ID3D11DeviceContext> deviceContext;

	// Shader structures
	ComPtr<ID3D11InputLayout> inputLayout;
	ComPtr<ID3D11VertexShader> vs;
	ComPtr<ID3D11PixelShader> ps;

	static ComPtr<ID3D10Blob> CompileShader(const wchar_t* filename, const SHADER_TYPE shaderType)
	{
		HRESULT hr = 0;
		ComPtr<ID3D10Blob> codeBlob, errBlob;
		switch (shaderType) {
		case SHADER_TYPE::ST_VERTEXSHADER:
			hr = D3DCompileFromFile(filename, nullptr, nullptr, "main_vs", "vs_4_0", 0, 0, codeBlob.ReleaseAndGetAddressOf(), errBlob.ReleaseAndGetAddressOf());
			break;
		case SHADER_TYPE::ST_PIXELSHADER:
			hr = D3DCompileFromFile(filename, nullptr, nullptr, "main_ps", "ps_4_0", 0, 0, codeBlob.ReleaseAndGetAddressOf(), errBlob.ReleaseAndGetAddressOf());
			break;
		}

		if (FAILED(hr)) {
			std::wostringstream wos;
			wos << "Failed to compile shader from " << filename << ".";
			MessageBox(NULL, wos.str().c_str(), L"Error", MB_ICONERROR | MB_OK);
		}

		// Write out the compile log if it exists -- it might have contained warnings.
		if (errBlob) {
			std::wostringstream wos;
			switch (shaderType) {
			case SHADER_TYPE::ST_VERTEXSHADER:
				wos << filename << ".vs.compile.log";
				break;
			case SHADER_TYPE::ST_PIXELSHADER:
				wos << filename << ".ps.compile.log";
				break;
			}
			D3DWriteBlobToFile(errBlob.Get(), wos.str().c_str(), TRUE);
		}

		return codeBlob;
	}
public:
	Direct3DShaderPass(const ComPtr<ID3D11Device>& inDevice,
		const ComPtr<ID3D11DeviceContext>& inDeviceContext,
		const wchar_t* shaderFilename, const D3D11_INPUT_ELEMENT_DESC ied[])
		: device(inDevice), deviceContext(inDeviceContext)
	{
		// Create shader blobs and shader objects.

		ComPtr<ID3D10Blob> vsBlob = CompileShader(shaderFilename, SHADER_TYPE::ST_VERTEXSHADER);
		ComPtr<ID3D10Blob> psBlob = CompileShader(shaderFilename, SHADER_TYPE::ST_PIXELSHADER);

		if (!vsBlob || !psBlob)
		{
			throw std::runtime_error("Shader compilation failed.");
		}

		this->device->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), nullptr,
			this->vs.ReleaseAndGetAddressOf());
		this->device->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), nullptr,
			this->ps.ReleaseAndGetAddressOf());

		// Set up input layout.

		this->device->CreateInputLayout(ied, 2, vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(),
			this->inputLayout.ReleaseAndGetAddressOf());
	}

	void Activate()
	{
		this->deviceContext->IASetInputLayout(this->inputLayout.Get());
		this->deviceContext->VSSetShader(this->vs.Get(), nullptr, 0);
		this->deviceContext->PSSetShader(this->ps.Get(), nullptr, 0);
	}
};


class Direct3DPresenter
{
	typedef struct
	{
		FLOAT x, y, z;
		FLOAT uv[2];
	} VERTEX;

	typedef struct
	{
		UINT enableDither;
		UINT enablePaletteViz;
		UINT enableHalfNHalf;
		UINT enableFixedBit;
		UINT fixedBitRShift;
		UINT fixedBitGShift;
		UINT fixedBitBShift;
		UINT paletteUsed;
		XMFLOAT3 paletteThresholds;
		FLOAT contrastAdjust;
		XMFLOAT4 palette[MAX_PALETTE_SIZE];
	} FILTER_CONSTANTS;

	typedef struct
	{
		UINT enableUpscale;
		UINT _pack1[3];
		XMFLOAT4 aspectAdjust;
	} FINAL_CONSTANTS;


	// Presenter variables
	int sourceWidth, sourceHeight;
	int presentWidth, presentHeight;
	bool presentSizeIsSourceSize;
	bool renderReady;
	bool enableDither, enablePaletteViz, enableUpscale, enableHalfNHalf;
	int fixedBitRDepth, fixedBitGDepth, fixedBitBDepth;
	float contrastAdjust;
	float aspectAdjustOverride;
	int64_t frameCount; // Presenter local frame count

	// Palette optimization variables (threaded)
	PaletteGeneration* paletteGeneration = nullptr;
	typedef struct
	{	
		Direct3DPresenter* presenter;
		FILTER_CONSTANTS filterData;
	} PGDATA;
	HANDLE pgThread;
	bool pgThreadLock;
	PGDATA pgData;

	// Direct3D Overall Interfaces
	ComPtr<ID3D11Device> device;
	ComPtr<ID3D11DeviceContext> deviceContext;
	ComPtr<IDXGISwapChain> swapchain;

	// Shaders and pipeline structures
	shared_ptr<Direct3DShaderPass> convPass;
	shared_ptr<Direct3DShaderPass> filterPass;
	shared_ptr<Direct3DShaderPass> finalPass;
	ComPtr<ID3D11SamplerState> linearSampler;
	ComPtr<ID3D11SamplerState> pointSampler;

	// Source texture
	ComPtr<ID3D11Texture2D> sourceTexture;
	D3D11_TEXTURE2D_DESC sourceTextureStoredDesc;
	ComPtr<ID3D11ShaderResourceView> sourceTextureView;

	// Converted color texture
	ComPtr<ID3D11Texture2D> convTexture;
	ComPtr<ID3D11ShaderResourceView> convTextureView;
	ComPtr<ID3D11RenderTargetView> convTarget;
	D3D11_VIEWPORT convViewport;

	// Readback texture
	ComPtr<ID3D11Texture2D> readbackTexture;

	// Filtered texture
	ComPtr<ID3D11Texture2D> filterTexture;
	ComPtr<ID3D11ShaderResourceView> filterTextureView;
	ComPtr<ID3D11RenderTargetView> filterTarget;
	D3D11_VIEWPORT filterViewport;

	// Presentation quad data
	ComPtr<ID3D11Buffer> pqVertexBuffer;
	ComPtr<ID3D11Buffer> pqIndexBuffer;

	// Constant sets
	shared_ptr<Direct3DConstants<FILTER_CONSTANTS>> filterConstants;
	shared_ptr<Direct3DConstants<FINAL_CONSTANTS>> finalConstants;

	// Backbuffers and viewports
	ComPtr<ID3D11RenderTargetView> backbuffer;
	D3D11_VIEWPORT windowViewport;

	void InitPipeline(void)
	{
		// Setup vertex description.

		D3D11_INPUT_ELEMENT_DESC ied[] =
		{
			{"POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0}
		};

		// Set up passes.
		
		this->convPass = make_shared<Direct3DShaderPass>(this->device, this->deviceContext, L"normal.hlsl", ied);
		this->filterPass = make_shared<Direct3DShaderPass>(this->device, this->deviceContext, L"filter.hlsl", ied);
		this->finalPass = make_shared<Direct3DShaderPass>(this->device, this->deviceContext, L"final.hlsl", ied);

		// Set up sampler state -- point sampler.

		D3D11_SAMPLER_DESC sd = {};
		sd.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
		sd.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		sd.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		sd.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
		sd.MinLOD = -FLT_MAX;
		sd.MaxLOD = FLT_MAX;
		sd.MaxAnisotropy = 1;

		this->device->CreateSamplerState(&sd, this->pointSampler.ReleaseAndGetAddressOf());

		// Set up sampler state -- linear sampler.

		sd.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;

		this->device->CreateSamplerState(&sd, this->linearSampler.ReleaseAndGetAddressOf());
	}

	void InitSourceTexture()
	{
		// Create the source texture according to the last seen description.

		this->device->CreateTexture2D(&sourceTextureStoredDesc, nullptr, this->sourceTexture.ReleaseAndGetAddressOf());

		// Setup shader resource view for texture.

		this->device->CreateShaderResourceView(this->sourceTexture.Get(), nullptr, this->sourceTextureView.ReleaseAndGetAddressOf());
	}

	void InitConvTexture(const int width, const int height)
	{
		// Create the converted color texture.

		D3D11_TEXTURE2D_DESC t2d = {};
		t2d.Width = width;
		t2d.Height = height;
		t2d.MipLevels = 1;
		t2d.ArraySize = 1;
		t2d.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		t2d.SampleDesc.Count = 1;
		t2d.Usage = D3D11_USAGE_DEFAULT;
		t2d.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;

		this->device->CreateTexture2D(&t2d, nullptr, this->convTexture.ReleaseAndGetAddressOf());

		// Setup shader resource view for texture.

		this->device->CreateShaderResourceView(this->convTexture.Get(), nullptr, this->convTextureView.ReleaseAndGetAddressOf());

		// Setup render target and viewport into texure.

		this->device->CreateRenderTargetView(this->convTexture.Get(), nullptr, this->convTarget.ReleaseAndGetAddressOf());

		this->convViewport = {};
		convViewport.Width = static_cast<FLOAT>(width);
		convViewport.Height = static_cast<FLOAT>(height);
	}

	void InitReadbackTexture(const int width, const int height)
	{
		// Create the intermediate result CPU readback texture.

		D3D11_TEXTURE2D_DESC t2d = {};
		t2d.Width = width;
		t2d.Height = height;
		t2d.MipLevels = 1;
		t2d.ArraySize = 1;
		t2d.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		t2d.SampleDesc.Count = 1;
		t2d.Usage = D3D11_USAGE_STAGING;
		t2d.CPUAccessFlags = D3D11_CPU_ACCESS_READ;

		this->device->CreateTexture2D(&t2d, nullptr, this->readbackTexture.ReleaseAndGetAddressOf());
	}

	void InitFilterTexture(const int width, const int height)
	{
		// Create the filter result texture.

		D3D11_TEXTURE2D_DESC t2d = {};
		t2d.Width = width;
		t2d.Height = height;
		t2d.MipLevels = 1;
		t2d.ArraySize = 1;
		t2d.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		t2d.SampleDesc.Count = 1;
		t2d.Usage = D3D11_USAGE_DEFAULT;
		t2d.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;

		this->device->CreateTexture2D(&t2d, nullptr, this->filterTexture.ReleaseAndGetAddressOf());

		// Setup shader resource view for texture.

		this->device->CreateShaderResourceView(this->filterTexture.Get(), nullptr, this->filterTextureView.ReleaseAndGetAddressOf());

		// Setup render target and viewport into texure.

		this->device->CreateRenderTargetView(this->filterTexture.Get(), nullptr, this->filterTarget.ReleaseAndGetAddressOf());
		
		this->filterViewport = {};
		filterViewport.Width = static_cast<FLOAT>(width);
		filterViewport.Height = static_cast<FLOAT>(height);
	}

	void InitPresentationQuad(void)
	{
		// Define presentation quad data.

		const VERTEX pqVertices[] =
		{
			{-1.0f, -1.0f, 0.0f, {0.0f, 1.0f}}, // bottom left  (0)
			{ 1.0f, -1.0f, 0.0f, {1.0f, 1.0f}}, // bottom right (1)
			{-1.0f,  1.0f, 0.0f, {0.0f, 0.0f}}, // top left     (2)
			{ 1.0f,  1.0f, 0.0f, {1.0f, 0.0f}}  // top right    (3)
		};
		const int pqIndices[] = { 0, 2, 3, 0, 3, 1 };

		// Create vertex buffer.

		D3D11_BUFFER_DESC bd = {};
		D3D11_SUBRESOURCE_DATA srd = {};
		bd.Usage = D3D11_USAGE_IMMUTABLE;
		bd.ByteWidth = sizeof(pqVertices);
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		srd.pSysMem = pqVertices;

		this->device->CreateBuffer(&bd, &srd, this->pqVertexBuffer.ReleaseAndGetAddressOf());

		// Create index buffer.

		bd = {};
		srd = {};
		bd.Usage = D3D11_USAGE_IMMUTABLE;
		bd.ByteWidth = sizeof(pqIndices);
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		srd.pSysMem = pqIndices;

		this->device->CreateBuffer(&bd, &srd, this->pqIndexBuffer.ReleaseAndGetAddressOf());
	}

	void InitConstants()
	{
		// Set up constant buffers.

		this->filterConstants = make_shared<Direct3DConstants<FILTER_CONSTANTS>>(device, deviceContext);
		this->finalConstants = make_shared<Direct3DConstants<FINAL_CONSTANTS>>(device, deviceContext);
	}

	static DWORD WINAPI PGThread(LPVOID lpData)
	{
		PGDATA* pgData = (PGDATA*)lpData;
		Direct3DPresenter* presenter = pgData->presenter;
		FILTER_CONSTANTS* filterData = &(pgData->filterData);

		// Bind the readback texture -- we use it here.

		D3D11_MAPPED_SUBRESOURCE ms;
		presenter->deviceContext->Map(presenter->readbackTexture.Get(), 0, D3D11_MAP_READ, 0, &ms);
		const BYTE* pData = (BYTE*)ms.pData;

		// Fill in fixed bit shifts if used, and enable in shader.
		if (presenter->fixedBitRDepth + presenter->fixedBitGDepth + presenter->fixedBitBDepth > 0) {
			filterData->enableFixedBit = 1;
			filterData->fixedBitRShift = max(0, 8 - presenter->fixedBitRDepth);
			filterData->fixedBitGShift = max(0, 8 - presenter->fixedBitGDepth);
			filterData->fixedBitBShift = max(0, 8 - presenter->fixedBitBDepth);
		}
		else {
			filterData->enableFixedBit = 0;
		}

		// Calculate the new palette -- not needed if running in a fixed bit depth mode.
		if (filterData->enableFixedBit) {
			filterData->paletteUsed = 0;
			filterData->paletteThresholds = XMFLOAT3(
				1.0f / (powf(2.0f, presenter->fixedBitRDepth) - 1.0f),
				1.0f / (powf(2.0f, presenter->fixedBitGDepth) - 1.0f),
				1.0f / (powf(2.0f, presenter->fixedBitBDepth) - 1.0f)
			);
		}
		else {
			filterData->paletteUsed = presenter->paletteGeneration->CalcPaletteFromFrame(
				pData, presenter->presentWidth, presenter->presentHeight, filterData->palette);
			filterData->paletteThresholds = presenter->paletteGeneration->GetPaletteThresholds();
		}

		// Fill in other minor things needed in filter constants.
		filterData->contrastAdjust = presenter->contrastAdjust;
		filterData->enableDither = presenter->enableDither;
		filterData->enablePaletteViz = presenter->enablePaletteViz;
		filterData->enableHalfNHalf = presenter->enableHalfNHalf;

		// Unbind the readback texture.
		presenter->deviceContext->Unmap(presenter->readbackTexture.Get(), 0);

		return 0;
	}

	void RecalcConstants()
	{
		// Fill new filter constants. Regenerate the palette asynchronously. 

		if (pgThreadLock == false)
		{
			this->pgData.presenter = this;
			this->pgThreadLock = true;
			this->pgThread = CreateThread(nullptr, 0, PGThread, &pgData, 0, nullptr);
		}
		else if (pgThreadLock == true && WaitForSingleObject(this->pgThread, 0) == WAIT_OBJECT_0)
		{
			CloseHandle(this->pgThread);
			this->filterConstants->UploadData(&this->pgData.filterData);
			this->pgThreadLock = false;
		}

		// Calculate aspect adjust based on current view setting.

		float sourceAspect = 0.0f;
		if (fabs(this->aspectAdjustOverride) > 1e-12f)
			sourceAspect = this->aspectAdjustOverride;
		else
			sourceAspect = (float) this->sourceWidth / (float) this->sourceHeight;
		float viewAspect = this->windowViewport.Width / this->windowViewport.Height;
		float aspectWidthMul = viewAspect > sourceAspect ? (sourceAspect / viewAspect) : 1.0f;
		float aspectHeightMul = viewAspect > sourceAspect ? 1.0f : (viewAspect / sourceAspect);
		float aspectWidthAdd = 0.0f; float aspectHeightAdd = 0.0f;

		// Fill new final constants.

		FINAL_CONSTANTS finalData = {};
		finalData.enableUpscale = this->enableUpscale;
		finalData.aspectAdjust = XMFLOAT4(aspectWidthMul, aspectHeightMul, aspectWidthAdd, aspectHeightAdd);
		this->finalConstants->UploadData(&finalData);
	}

public:
	Direct3DPresenter(HWND hwnd, PaletteGeneration* inPaletteGeneration, int inPresentWidth, int inPresentHeight,
		bool inDither, bool inPaletteViz, bool inUpscale)
		: presentWidth(inPresentWidth), presentHeight(inPresentHeight), presentSizeIsSourceSize(false),
		renderReady(false), frameCount(0), paletteGeneration(inPaletteGeneration), pgThreadLock(false),
		enableDither(inDither), enablePaletteViz(inPaletteViz), enableUpscale(inUpscale), enableHalfNHalf(false),
		fixedBitRDepth(0), fixedBitGDepth(0), fixedBitBDepth(0),
		contrastAdjust(1.0f), aspectAdjustOverride(0.0f)
	{
		if (presentWidth == 0 || presentHeight == 0)
			presentSizeIsSourceSize = true;

		// Initalize Direct3D device, context and swapchain.

		DXGI_SWAP_CHAIN_DESC scd = {};
		scd.BufferCount = 1;
		scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		scd.OutputWindow = hwnd;
		scd.SampleDesc.Count = 1;
		scd.Windowed = TRUE;

		D3D11CreateDeviceAndSwapChain(
			nullptr,
			D3D_DRIVER_TYPE_HARDWARE,
			nullptr,
			0,
			nullptr,
			0,
			D3D11_SDK_VERSION,
			&scd,
			this->swapchain.ReleaseAndGetAddressOf(),
			this->device.ReleaseAndGetAddressOf(),
			nullptr,
			this->deviceContext.ReleaseAndGetAddressOf()
		);

		// Create render target and viewport.
		HandleResize(hwnd);

		// Set up graphics pipeline and structures.
		InitPipeline();
		InitPresentationQuad();
		InitConstants();
	}

	~Direct3DPresenter()
	{
		// Wait for the palette generation thread to exit before we cleanup fully.
		WaitForSingleObject(this->pgThread, INFINITE);
	}

	ComPtr<IDXGIDevice> GetDXGIDevice()
	{
		ComPtr<IDXGIDevice> dxgiDevice;
		device->QueryInterface(dxgiDevice.ReleaseAndGetAddressOf());
		return dxgiDevice;
	}

	void SetNewPaletteGeneration(PaletteGeneration* inPaletteGeneration)
	{
		// Wait for the PG thread to terminate before setting the new palette generation class.
		WaitForSingleObject(this->pgThread, INFINITE);
		this->paletteGeneration = inPaletteGeneration;
	}
	void ForcePaletteUpdate()
	{
		if (this->paletteGeneration != nullptr)
			this->paletteGeneration->ForceUpdate();
	}

	void SetFixedBitDepths(int inRDepth, int inGDepth, int inBDepth)
	{
		assert(inRDepth >= 0 && inRDepth <= 8);
		assert(inGDepth >= 0 && inGDepth <= 8);
		assert(inBDepth >= 0 && inBDepth <= 8);
		this->fixedBitRDepth = inRDepth;
		this->fixedBitGDepth = inGDepth;
		this->fixedBitBDepth = inBDepth;
	}
	
	void ToggleDither() { this->enableDither = !this->enableDither; }
	void TogglePaletteViz() { this->enablePaletteViz = !this->enablePaletteViz; }
	void ToggleUpscale() { this->enableUpscale = !this->enableUpscale; }
	void ToggleEnableHalfNHalf() { this->enableHalfNHalf = !this->enableHalfNHalf; }
	void AdjustContrast(float delta) { this->contrastAdjust = max(0.3f, min(2.0f, this->contrastAdjust + delta)); }
	void SetAspectAdjustOverride(float newValue) { this->aspectAdjustOverride = newValue; }

	void SetNewPresentSize(int newPresentWidth, int newPresentHeight)
	{
		this->presentWidth = newPresentWidth;
		this->presentHeight = newPresentHeight;
		if (presentWidth == 0 || presentHeight == 0)
			presentSizeIsSourceSize = true;
		else
			presentSizeIsSourceSize = false;
		// Need to make render as unready to force recreation of textures.
		this->renderReady = false;
	}

	void HandleResize(HWND hwnd)
	{
		// Destroy old primary backbuffer.

		this->deviceContext->OMSetRenderTargets(0, nullptr, nullptr);

		// Ensure good sizing of swapchain backbuffers.
		this->backbuffer.ReleaseAndGetAddressOf();
		this->swapchain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0);

		// Create new render target (primary backbuffer).

		ComPtr<ID3D11Texture2D> pBuffer;
		this->swapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)pBuffer.ReleaseAndGetAddressOf());

		this->device->CreateRenderTargetView(pBuffer.Get(), nullptr, this->backbuffer.ReleaseAndGetAddressOf());

		// Set up window viewport.

		RECT rc;
		GetClientRect(hwnd, &rc);
		this->windowViewport = {};
		windowViewport.Width = static_cast<FLOAT>(rc.right - rc.left);
		windowViewport.Height = static_cast<FLOAT>(rc.bottom - rc.top);

		// Re-render current frame.
		RenderFrame();
	}

	void CopyToSourceTexture(ID3D11Texture2D* newSourceTexture, D3D11_TEXTURE2D_DESC newTextureDesc, D3D11_BOX* sourceBox)
	{
		// Get width/height from source box.
		UINT newSourceWidth = (sourceBox->right - sourceBox->left);
		UINT newSourceHeight = (sourceBox->bottom - sourceBox->top);
		
		// Check if we need to recreate source.
		if (newSourceWidth != sourceTextureStoredDesc.Width ||
			newSourceHeight != sourceTextureStoredDesc.Height ||
			newTextureDesc.Format != sourceTextureStoredDesc.Format ||
			!renderReady)
		{
			// Copy over parameters from newly given description.
			sourceTextureStoredDesc = {};
			sourceTextureStoredDesc.Width = newSourceWidth;
			sourceTextureStoredDesc.Height = newSourceHeight;
			sourceTextureStoredDesc.MipLevels = 1;
			sourceTextureStoredDesc.ArraySize = 1;
			sourceTextureStoredDesc.Format = newTextureDesc.Format;
			sourceTextureStoredDesc.SampleDesc.Count = 1;
			sourceTextureStoredDesc.Usage = D3D11_USAGE_DEFAULT;
			sourceTextureStoredDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;

			// Calculate final source width/height in class.
			this->sourceWidth = newSourceWidth;
			this->sourceHeight = newSourceHeight;

			if (presentSizeIsSourceSize)
			{
				this->presentWidth = this->sourceWidth;
				this->presentHeight = this->sourceHeight;
			}

			InitSourceTexture();
			InitConvTexture(this->presentWidth, this->presentHeight);
			InitReadbackTexture(this->presentWidth, this->presentHeight);
			InitFilterTexture(this->presentWidth, this->presentHeight);
		}

		this->deviceContext->CopySubresourceRegion(this->sourceTexture.Get(), 0, 0, 0, 0, newSourceTexture, 0, sourceBox);

		// Video texture filled, so we are now finally ready to render.
		renderReady = true;
	}

	void RenderFrame()
	{
		// Early bath if aren't fully ready to render yet.
		if (!renderReady)
			return;

		// Clear the screen.
		const FLOAT clearColor[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
		this->deviceContext->ClearRenderTargetView(this->backbuffer.Get(), clearColor);

		// Setup input assembler.
		UINT stride = sizeof(VERTEX);
		UINT offset = 0;
		this->deviceContext->IASetVertexBuffers(0, 1, this->pqVertexBuffer.GetAddressOf(), &stride, &offset);
		this->deviceContext->IASetIndexBuffer(this->pqIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

		// Select appropriate primitive type -- triangles.
		this->deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		// Run the conv pass -- render video texture into converted color texture.
		this->convPass->Activate();
		this->deviceContext->PSSetSamplers(0, 1, this->linearSampler.GetAddressOf());
		this->deviceContext->PSSetShaderResources(0, 1, this->sourceTextureView.GetAddressOf());
		this->deviceContext->RSSetViewports(1, &this->convViewport);
		this->deviceContext->OMSetRenderTargets(1, this->convTarget.GetAddressOf(), nullptr);
		this->deviceContext->DrawIndexed(6, 0, 0);

		// Debind render target.
		this->deviceContext->OMSetRenderTargets(0, nullptr, nullptr);

		// Copy conv pass texture for readback (unless locked because async palette generation is still running).
		if (!this->pgThreadLock)
			this->deviceContext->CopyResource(this->readbackTexture.Get(), this->convTexture.Get());

		// Recaclulate dynamic constant buffers now that the readback texture is available.
		this->RecalcConstants();

		// Run the filter pass -- render converted color texture into filter texture.
		this->filterPass->Activate();
		this->filterConstants->BindPS(0);
		this->deviceContext->PSSetSamplers(0, 1, this->pointSampler.GetAddressOf());
		this->deviceContext->PSSetShaderResources(0, 1, this->convTextureView.GetAddressOf());
		this->deviceContext->RSSetViewports(1, &this->filterViewport);
		this->deviceContext->OMSetRenderTargets(1, this->filterTarget.GetAddressOf(), nullptr);
		this->deviceContext->DrawIndexed(6, 0, 0);

		// Debind render target.
		this->deviceContext->OMSetRenderTargets(0, nullptr, nullptr);

		// Run the final pass.
		this->finalPass->Activate();
		this->finalConstants->BindVS(0);
		this->finalConstants->BindPS(0);
		this->deviceContext->PSSetSamplers(0, 1, this->pointSampler.GetAddressOf());
		this->deviceContext->PSSetShaderResources(0, 1, this->filterTextureView.GetAddressOf());
		this->deviceContext->RSSetViewports(1, &this->windowViewport);
		this->deviceContext->OMSetRenderTargets(1, this->backbuffer.GetAddressOf(), nullptr);
		this->deviceContext->DrawIndexed(6, 0, 0);

		// Debind render target.
		this->deviceContext->OMSetRenderTargets(0, nullptr, nullptr);

		// Present the results and update presenter local frame counter.
		this->swapchain->Present(0, 0);
		this->frameCount++;
	}
};

struct ProgramState
{
	shared_ptr<PaletteGeneration> palGen;
	shared_ptr<Direct3DPresenter> presenter;
	std::wstring capturePartial;
	double windowScore;
	HWND programWindow, captureWindow;
	shared_ptr<WGCCapture> capture;

	// Moved here so resize code can reference it.
	int downscaleWidth = 0, downscaleHeight = 0;
	bool topMost = false;

	static BOOL CALLBACK EnumWindowProc(HWND testHwnd, LPARAM lParam)
	{
		ProgramState* programState = (ProgramState*)lParam;

		// Get information about our window.
		wchar_t classBuffer[1024], titleBuffer[1024];
		GetClassName(testHwnd, classBuffer, 1024);
		GetWindowText(testHwnd, titleBuffer, 1024);

		// Fuzzy match on class and window names.
		double classScore = fuzz::partial_ratio(classBuffer, programState->capturePartial);
		double titleScore = fuzz::partial_ratio(titleBuffer, programState->capturePartial) * 2.0;
		double newScore = max(classScore, titleScore);
		// Prefer visible windows.
		if (IsWindowVisible(testHwnd))
			newScore *= 2.0;

		if (newScore > programState->windowScore)
		{
			programState->windowScore = newScore;
			programState->captureWindow = testHwnd;
		}

		return TRUE;
	}

	bool AttemptFindCaptureWindow(bool dieIfNotFound)
	{
		if (capture)
			capture.reset();

		// Select which window we intend to capture.
		captureWindow = nullptr;
		if (!capturePartial.empty())
		{
			// We have a good partial.. try window enumeration.
			this->windowScore = 0;
			EnumWindows(EnumWindowProc, (LPARAM)this);
		}
		else
		{
			// TODO: try a stack of defaults here.
		}

		// If we still don't have a valid capture window, die.
		if (!captureWindow || !IsWindow(captureWindow) || !IsWindowVisible(captureWindow))
		{
			if (dieIfNotFound)
			{
				MessageBoxW(nullptr, L"Could not find a visible window to capture.\nUsage: ditherSystem.exe <window title or class name>",
					L"No window!", MB_ICONERROR | MB_OK);
				PostQuitMessage(0);
			}
			else
				return false;
		}

		// If we have a capture window, and our program window is not fullscreen, resize to match the capture window.
		DWORD dwStyle = GetWindowLong(programWindow, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW || topMost)
		{
			RECT crect = {};
			if (downscaleWidth > 0 && downscaleHeight > 0) {
				crect.right = downscaleWidth;
				crect.bottom = downscaleHeight;
			} else {
				DwmGetWindowAttribute(captureWindow, DWMWA_EXTENDED_FRAME_BOUNDS, &crect, sizeof(crect));
			}

			RECT wrect, xrect;
			SetProcessDpiAwarenessContext(DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE);
			GetWindowRect(programWindow, &wrect);
			DwmGetWindowAttribute(programWindow, DWMWA_EXTENDED_FRAME_BOUNDS, &xrect, sizeof(xrect));

			POINT wtl = { wrect.left, wrect.top };
			POINT wbr = { wrect.right, wrect.bottom };
			POINT xtl = { xrect.left, xrect.top };
			POINT xbr = { xrect.right, xrect.bottom };

			PhysicalToLogicalPointForPerMonitorDPI(programWindow, &xtl);
			PhysicalToLogicalPointForPerMonitorDPI(programWindow, &xbr);
			int newWidth  = (crect.right - crect.left) + (xtl.x - wtl.x) + (wbr.x - xbr.x);
			int newHeight = (crect.bottom - crect.top) + (xtl.y - wtl.y) + (wbr.y - xbr.y);

			SetWindowPos(programWindow, nullptr, 0, 0, newWidth, newHeight, SWP_NOMOVE | SWP_NOZORDER);
			presenter->HandleResize(programWindow);
		}

		// Setup Graphics Capture.
		capture = make_shared<WGCCapture>(presenter->GetDXGIDevice(), captureWindow);
		return true;
	}

	ProgramState(HWND hwnd, LPWSTR lpCmdLine)
	{
		// Save program window.
		programWindow = hwnd;

		// Process command line.
		capturePartial = std::wstring();
		bool enableDither = true, enablePaletteViz = false, enableUpscale = false;
		bool enableFixedBit = false; FBD_SETUP_TYPE fixedBitSetup;
		float aspectAdjustOverride = 0.0f;
		if (wcslen(lpCmdLine) > 0)
		{
			int argc; LPWSTR* wargv = CommandLineToArgvW(lpCmdLine, &argc);
			
			for (unsigned i = 0; i < (unsigned)argc; i++)
			{
				// Check for flags.
				LPWSTR curArg = wargv[i];
				if (wcslen(curArg) >= 2 && !wcsncmp(curArg, L"-", 1))
				{
					LPWSTR flag = curArg + 1;
					auto checkFlag = [flag](wchar_t shortForm, wchar_t* longForm) -> bool
					{
						return (wcslen(flag) == 1 && flag[0] == shortForm) || !_wcsicmp(flag, longForm);
					};
					auto getFlagArg = [&i, argc, wargv]() -> LPWSTR
					{
						if ((i + 1) >= (unsigned)argc) // Check for potential argument.
							return nullptr;
						i += 1; LPWSTR flagArg = wargv[i];

						// Lowercase the argument.
						for (LPWSTR ch = flagArg; *ch != L'\0'; ch++)
							*ch = std::tolower(*ch);

						return flagArg;
					};
					
					// -p: palette specification
					if (checkFlag(L'p', L"palette"))
					{
						LPWSTR flagArg = getFlagArg();

						// Otherwise, fuzzy match on palette setup slugs.
						double bestScore = 0.0; double newScore;
						PG_SETUP_TYPE bestSetup = {};
						for (auto setup : PG_SETUP) {
							newScore = fuzz::partial_ratio(flagArg, setup.setupSlug);
							newScore += fuzz::ratio(flagArg, setup.setupSlug);
							if (newScore > bestScore)
							{
								bestScore = newScore;
								bestSetup = setup;
							}
						}

						// Actually create the new palette generation class.
						PaletteGeneration* newPalGen = bestSetup.setupFunc();
						if (newPalGen)
							palGen = shared_ptr<PaletteGeneration>(newPalGen);
					}

					// -w: specify downscale width
					else if (checkFlag(L'w', L"width"))
					{
						LPWSTR flagArg = getFlagArg();
						downscaleWidth = _wtoi(flagArg);
					}

					// -h: specify downscale height
					else if (checkFlag(L'h', L"height"))
					{
						LPWSTR flagArg = getFlagArg();
						downscaleHeight = _wtoi(flagArg);
					}

					// -c: disable dithering
					else if (checkFlag(L'c', L"nodither"))
						enableDither = false;

					// -v: enable palette visualization
					else if (checkFlag(L'v', L"visualize"))
						enablePaletteViz = true;

					// -s: enable soft upscale
					else if (checkFlag(L's', L"upscale"))
						enableUpscale = true;

					// -a: override final aspect ratio
					else if (checkFlag(L'a', L"aspect"))
					{
						LPWSTR flagArg = getFlagArg();
						aspectAdjustOverride = (float)_wtof(flagArg);
					}
					
					// enable fixed depth dithers
					else
					{						
						for (auto setup : FBD_SETUP) {
							if (checkFlag(setup.shortArg, setup.longArg)) {
								fixedBitSetup = setup;
								enableFixedBit = true;
								break;
							}
						}
					}
				}
				// Otherwise, set capture partial.
				else if (capturePartial.empty())
					capturePartial = curArg;
				// Ignore unknown arguments for now.
			}
		}

		// Default palette generation if not yet created.
		if (!palGen)
			palGen = shared_ptr<PaletteGeneration>(PG_SETUP[0].setupFunc());

		// Setup Direct3D.
		presenter = make_shared<Direct3DPresenter>(hwnd, palGen.get(), downscaleWidth, downscaleHeight,
			enableDither, enablePaletteViz, enableUpscale);
		if (fabs(aspectAdjustOverride) > 1e-12f)
			presenter->SetAspectAdjustOverride(aspectAdjustOverride);
		if (enableFixedBit) {
			presenter->SetFixedBitDepths(fixedBitSetup.rBits, fixedBitSetup.gBits, fixedBitSetup.bBits);
		}

		// Attempt to find a window to capture -- this will also set up the capture object.
		AttemptFindCaptureWindow(true);
	}
};

int WINAPI wWinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPWSTR lpCmdLine, _In_ int nCmdShow){
	try {
		// Register our window class.

		const wchar_t CLASS_NAME[] = L"Dither System Window Class";

		WNDCLASS wc = {};
		wc.lpfnWndProc = WindowProc;
		wc.hInstance = hInstance;
		wc.lpszClassName = CLASS_NAME;

		RegisterClass(&wc);

		// Create the window.

		int viewWidth = NOMINAL_VIEW_WIDTH;
		int viewHeight = NOMINAL_VIEW_HEIGHT;
		RECT wr = { 0, 0, viewWidth, viewHeight };
		AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, FALSE);

		HWND hwnd = CreateWindowEx(
			0,
			CLASS_NAME,
			L"Dither System",
			WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT, CW_USEDEFAULT,
			wr.right - wr.left, wr.bottom - wr.top,
			nullptr,
			nullptr,
			hInstance,
			nullptr
		);

		if (hwnd == nullptr)
		{
			return 0;
		}

		ShowWindow(hwnd, nCmdShow);

		// Register an return hotkey for topmost mode (Ctrl-Alt-T).
		RegisterHotKey(hwnd, 0, MOD_ALT | MOD_CONTROL, 'T');

		// Create our program state -- it's constructor will initialize other classes we use.
		ProgramState programState = ProgramState(hwnd, lpCmdLine);
		SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)&programState);
		auto presenter = shared_ptr<Direct3DPresenter>(programState.presenter);
		auto capture = shared_ptr<WGCCapture>(programState.capture);

		// Run main loop.

		MSG msg = {};
		bool running = true;
		while (running)
		{
			// Pump windows meesage loop.
			while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
				if (msg.message == WM_QUIT)
				{
					running = false;
					break;
				}
			}

			
			WGCCapture::CaptureReadiness captureReady = capture->IsCaptureReady();
			if (captureReady == WGCCapture::CaptureReadiness::CAPTURE_READY)
			{
				D3D11_BOX sourceBox = capture->GetClientSubarea();
				presenter->CopyToSourceTexture(capture->GetFrameSurface().Get(),
					capture->GetFrameSurfaceDesc(), &sourceBox);
				presenter->RenderFrame();
				capture->CaptureTaken();
			}
			else if (captureReady == WGCCapture::CaptureReadiness::CAPTURE_DEAD)
			{
				// Attempt to recapture the window.
				if (programState.AttemptFindCaptureWindow(false))
					capture = shared_ptr<WGCCapture>(programState.capture);
				else
				{
					running = false;
					break;
				}
			}
		}
	}
	catch (const std::exception&) {
		return 0;
	}
	return 0;
}


LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	ProgramState* programState = (ProgramState*)GetWindowLongPtr(hwnd, GWLP_USERDATA);
	static std::vector<bool> keyState = std::vector<bool>(256, false);

	switch (uMsg)
	{
	case WM_CLOSE:
		PostQuitMessage(0);
		return 0;
	case WM_HOTKEY:
		if (wParam == 0)
		{
			HWND focusedWindow = GetForegroundWindow();
			if (focusedWindow == hwnd)
			{
				// Switch focus to game.
				SetForegroundWindow(programState->captureWindow);
			}
			else
			{
				// Restore focus to us.
				SetForegroundWindow(hwnd);
			}
		}
		return 0;
	case WM_KEYDOWN:
		keyState[wParam] = true;
		switch (wParam)
		{
		case 'T':
		{
			programState->topMost = !programState->topMost;

			DWORD dwExStyle = GetWindowLong(hwnd, GWL_EXSTYLE);
			const DWORD tmFlags = WS_EX_TRANSPARENT | WS_EX_LAYERED | WS_EX_NOACTIVATE | WS_EX_APPWINDOW;
			if (programState->topMost) {
				SetWindowLong(hwnd, GWL_EXSTYLE, dwExStyle | tmFlags);
				SetForegroundWindow(programState->captureWindow); // Switch focus to game.
			}
			else {
				SetWindowPos(hwnd, HWND_BOTTOM, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
				SetWindowPos(hwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
				SetWindowLong(hwnd, GWL_EXSTYLE, dwExStyle & ~tmFlags);
			}
			ShowCursor(!programState->topMost);
			
			// Deliberate fallthrough to 'F' case here..
		}
		case 'F':
		{
			static WINDOWPLACEMENT wpPrev = { sizeof(wpPrev) };
			DWORD dwStyle = GetWindowLong(hwnd, GWL_STYLE);
			if (dwStyle & WS_OVERLAPPEDWINDOW || programState->topMost) // Switching to fullscreen..
			{
				GetWindowPlacement(hwnd, &wpPrev);

				MONITORINFO mi = { sizeof(mi) };
				GetMonitorInfo(MonitorFromWindow(hwnd, MONITOR_DEFAULTTOPRIMARY), &mi);

				SetWindowLong(hwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(hwnd, programState->topMost ? HWND_TOPMOST : HWND_TOP,
					mi.rcMonitor.left, mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
			}
			else // Switching back to windowed..
			{
				SetWindowLong(hwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
				SetWindowPlacement(hwnd, &wpPrev);
				SetWindowPos(hwnd, nullptr, 0, 0, 0, 0,
					SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
			}
			break;
		}
		case 'Q':
			PostQuitMessage(0);
			break;
		case 'P':
		{
			static int pgOption = 0;

			PaletteGeneration* newPalGen = PG_SETUP[pgOption].setupFunc();
			programState->presenter->SetFixedBitDepths(0, 0, 0);
			programState->presenter->SetNewPaletteGeneration(newPalGen);
			programState->palGen.reset(); // Important to free old palette generation here.
			programState->palGen = shared_ptr<PaletteGeneration>(newPalGen);

			pgOption = (pgOption + 1) % PG_SETUP.size();
			break;
		}
		case 'C': { programState->presenter->ToggleDither(); break; }
		case 'V': { programState->presenter->TogglePaletteViz(); break; }
		case 'S': { programState->presenter->ToggleUpscale(); break; }
		case 'R':
		{
			static int pixelResSelect = -1;
			pixelResSelect = (pixelResSelect + 1) % 4;
			if (pixelResSelect == 0)
				programState->presenter->SetNewPresentSize(0, 0);
			else if(pixelResSelect == 1)
				programState->presenter->SetNewPresentSize(320, 200);
			else if (pixelResSelect == 2)
				programState->presenter->SetNewPresentSize(512, 384);
			else if (pixelResSelect == 3)
				programState->presenter->SetNewPresentSize(640, 480);
			break;
		}
		case 'H': { programState->presenter->ToggleEnableHalfNHalf(); break; }
		case 'U': { programState->presenter->ForcePaletteUpdate(); break; }
		case VK_OEM_MINUS: { programState->presenter->AdjustContrast(-0.05f); break; }
		case VK_OEM_PLUS: { programState->presenter->AdjustContrast(+0.05f); break; }
		default: {
			// Check for fixed-bit selectors.
			for (auto setup : FBD_SETUP) {
				char checkKey; wctomb_s(nullptr, &checkKey, 1, setup.shortArg);
				if (wParam == checkKey) {
					programState->presenter->SetFixedBitDepths(setup.rBits, setup.gBits, setup.bBits);
				}
			}
		}
		}
		return 0;
	case WM_KEYUP:
		keyState[wParam] = false;
		return 0;
	case WM_SIZE:
		if (programState != nullptr && programState->presenter != nullptr)
			programState->presenter->HandleResize(hwnd);
		return 0;
	}
	return DefWindowProc(hwnd, uMsg, wParam, lParam);
}