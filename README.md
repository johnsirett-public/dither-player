﻿# Dither System BETA

Experimental project demonstrating real-time palette quantization and dithering for animated sources.
Works for any Windows program you can run in windowed mode; media player, video games, web browser, etc.

## System Requirements

- Windows 10, version 1803 or later (required as this uses the modern Windows Graphics Capture API).
- Fast modern GPU which supports Direct3D 10+.

## General Usage

Start your game or other program, then run ditherSystem from a command line, as `ditherSystem.exe <windowName> [options]`

Options are:

- `windowName` indicates which window to capture. Partial title names and partial window class names are both accepted. Case-sensitive.
- `-p [palette]` or `-palette [palette]` specifies what palette or palette-optimizing algorithm to use. Default is optimized 256-color palette.
    - See the source file "fixedPalettes.h" for all available options here. Try `obramac`, `16blackandwhite`, `gameboy`, `commodore64`, `egabios`, `vga16optimize` or `doom1993`, see FixedPalettes.h for others.
- `-w [width]` or `-width [width]`, along with `-h [height]` or `-height [height]` specify a resolution to be used to downscale before dithering. You must specify both.
- `-c` or `-nodither` will disable the dithering effect, and quantize the closest palette color for all pixels. Expect color banding artifacts.
- Certain numbered parameters will disable palette optimization, and quantize colors to the closest color in a fixed-bit colorspace.
    - `-9` or `-9bit`  -> 9-bit colorspace  (3/3/3)
    - `-2` or `-12bit` -> 15-bit colorspace (4/4/4)
    - `-5` or `-15bit` -> 15-bit colorspace (5/5/5)
    - `-6` or `-16bit` -> 16-bit colorspace (5/6/5)
    - `-8` or `-18bit` -> 18-bit colorspace (6/6/6)
    - `-0` or `-24bit` -> 24-bit colorspace (8/8/8). In effect, this disables colorspace reduction.
- `-v` will cause a visualization of the current palette to be rendered at the bottom of the screen.
- `-s` will enable Lanczos filtering of the final upscaled output. By default output is displayed nearest-neighbor; this works best if the upscaled image is an integer multiple of the downscaled and dithered image.
- `-a [decimal aspect ratio]` will allow you to override the aspect ratio of the final upscaled output. By default, the aspect ratio used will be matched to the aspect of the source window. For 4:3 specify 1.333, 16:9 specify 1.778, 16:10 specify 1.6.

Once the ditherSystem window is open, press 'T' start playing full-screen, or 'Ctrl-Alt-T' to start playing as-is. Press 'Ctrl-Alt-T' again to return to the ditherSystem window, then press 'Q' to quit.

## Try these examples!

- Per-frame optimized 256-color VGA-like palettes (default): `ditherSystem.exe <windowName>`.
- Subtle 16-bit dithering, looks good at 640x480: `ditherSystem.exe <windowName> -6`.
- Simulate VGA's famous Mode 13h: `ditherSystem.exe <windowName> -p vgaoptimize -w 320 -h 200`.
- Simulate EGA's infamous Mode 0Dh: `ditherSystem.exe <windowName> -p egabios -w 320 -h 200`.
- リサフランク420 / 現代のコンピュー: `ditherSystem.exe <windowName> -p 4obramac -w 512 -h 342`.
- DOOM!!: `ditherSystem.exe <windowName> -p doom1993 -w 320 -h 200`.

## Advanced Usage

While the program is running, and the ditherSystem window is active:

- 'T' toggles top-most / game mode. This will do the following; make the window topmost on your screen, make it transparent to all input, and hide the cursor. This is useful for single-screen usage, as it will let you play a game in the background behind the dithered image.
    - 'Ctrl-Alt-T' will swap focus between the dither system window and the game window. This is useful for 'getting back control' after entering top-most mode and then switching to your game.
- 'F' toggles fullscreen display.
- 'P' cycles different palettes.
- 'U' will force optimized palettes to refresh.
- Fixed bit depth modes; '9', '2', '5', '6', '8', '0'. These behave like the same-named command-line parameters above.
- 'C' toggles dithering / banded output.
- 'V' toggles palette visualization display.
- 'S' toggles filtering on upscaled output.
- 'R' cycles the downscale resolution between a number of fixed resolutions.
- 'H' toggles half'n'half mode. The paletteization effect will not be applyed on the right side of the upscaled image, allowing for quick comparision between paletteized results and original colors.
- 'Q' quits the program.
- '-' and '+' keys adjust contrast prior to dithering. This may be needed for the monochrome palettes.